/* 
 * Magic Mirror Module: mmIslamicReminders
 * 
 * dihsaR damhA
 * 
 */

Module.register("mmIslamicReminders",{

    // Module config defaults.
    defaults: {
        updateInterval: 300,    // Value is in SECONDS
        fadeSpeed: 4,           // How fast (in SECONDS) to fade out and back in when changing Hadiths
        category: "random",     // Category to use
        remoteFile: "IslamicReminders.json",       // Provide reminders in a file
		islamicCollection: {
            Hadiths: [
                // Hadith Collections from - https://topislamic.com/collection-short-hadith/
                "The Prophet (saws) said, \"All drinks that produce intoxication are Haram (forbidden to drink).\" ~ Sahih Bukhari",
                "Whenever the Prophet (saws) got up at night, he used to clean his mouth with Siwak. ~ Sahih Bukhari",
                "Allah's Apostle (saws) said, \"If anyone of you enters a mosque, he should pray two Rakat before sitting.\" ~ Sahih Bukhari",
                "Allah's Apostle (saws) said, \"Whenever you hear the Adhan, say what the Muadhin is saying.\" ~ Sahih Bukhari",
                "The Prophet (saws) said, \"If supper is served, and Iqama is pronounced one should start with the supper.\" ~ Sahih Bukhari",
            ],
        },
    },

    // Define start sequence.
    start: function() {
        Log.info("Starting module: " + this.name);

        this.lastHadithIndex = -1;

        // Schedule update timer.
        var self = this;

        // Check if using a file for the Reminders
        if (this.config.remoteFile !== null) {
			this.remindersFile(function(response) {
				self.config.islamicCollection = JSON.parse(response);
				self.updateDom();
			});
		}

        // Set Interval for refresh
        setInterval(function() {
            self.updateDom(self.config.fadeSpeed * 1000);
        }, this.config.updateInterval * 1000);
    },

    /* randomIndex(hadithsParameter)
     * Generate a random index for a list of hadithsParameter.
     *
     * argument hadithsParameter Array<String> - Array with Hadiths.
     *
     * return Number - Random index.
     */
    randomIndex: function(hadithsParameter) {
        if (hadithsParameter.length === 1) {
            return 0;
        }

        var generate = function() {
            return Math.floor(Math.random() * hadithsParameter.length);
        };

        var hadithIndex = generate();

        while (hadithIndex === this.lastHadithIndex) {
            hadithIndex = generate();
        }

        this.lastHadithIndex = hadithIndex;

        return hadithIndex;
    },

    /* 
     * getArray()
	 * Retrieve an array of islamicCollections either of a specific category or all available ones.
	 *
	 * return islamicCollections Array<String> - Array with islamicCollections.
	 */
	getArray: function() {
        if (this.config.category === 'random') {
            return this.config.islamicCollection[Object.keys(this.config.islamicCollection)[Math.floor(Math.random() * Object.keys(this.config.islamicCollection).length)]];
        } else {
            return this.config.islamicCollection[this.config.category];
        }
    },

    /* remindersFile(callback)
	 * Retrieve a file from the local filesystem
	 */
	remindersFile: function(callback) {
		var xobj = new XMLHttpRequest(),
			isRemote = this.config.remoteFile.indexOf("http://") === 0 || this.config.remoteFile.indexOf("https://") === 0,
			path = isRemote ? this.config.remoteFile : this.file(this.config.remoteFile);
		xobj.overrideMimeType("application/json");
		xobj.open("GET", path, true);
		xobj.onreadystatechange = function() {
			if (xobj.readyState === 4 && xobj.status === 200) {
				callback(xobj.responseText);
			}
		};
		xobj.send(null);
	},

    /* 
     * randomHadith()
     * Retrieve a random quote.
     *
     * return quote string - A quote.
     */
    randomHadith: function() {
        var hadithsLocal = this.getArray();
        var index = this.randomIndex(hadithsLocal);
        return hadithsLocal[index].split(" ~ ");
    },

    // Override dom generator.
    getDom: function() {
        var hadithText = this.randomHadith();

        var hadithMessage = hadithText[0];
        var hadithSource = hadithText[1];

        var wrapper = document.createElement("div");

        var hadithDoc = document.createElement("div");
        hadithDoc.className = "bright medium light";
        hadithDoc.style.textAlign = 'center';
        hadithDoc.style.margin = '0 auto';
        hadithDoc.style.maxWidth = '50%';
        // hadithDoc.style.fontFamily = "Noto Sans Arabic UI";
        // hadithDoc.style.fontStyle = "Regular"
        hadithDoc.innerHTML = hadithMessage;

        wrapper.appendChild(hadithDoc);

        var sourceDoc = document.createElement("div");
        sourceDoc.className = "light small dimmed";
        // sourceDoc.innerHTML = "~ " + hadithSource;
        sourceDoc.innerHTML = hadithSource;

        wrapper.appendChild(sourceDoc);

        return wrapper;
    }

});