# mmIslamicReminders

`mmIslamicReminders` is a [`MagicMirror2`](https://magicmirror.builders) module of aHadith, and Duas collection, displaying a random aHadith or Dua or from all or a specific category.

See the section on [`Updating Reminders`](#updating-reminders) below to update with your own list of Hadith or Duas.

![Screenshot](Screenshot.png)

## Installing the module
Clone this repository in your `~/MagicMirror/modules/` folder
````javascript
git clone https://gitlab.com/dihsaR/mmIslamicReminders.git
````

## Using the module
To use this module, add it to the `modules` array in the `config/config.js` file:
````javascript
modules: [
    ... // Other modules

    {
        module: "mmIslamicReminders",
        position: "lower_third",
        config: {
                // The config property is optional.
                // See below for available configurable options
        }
    },

    ... // Other modules
]
````

## Configuration options
Configure `mmIslamicReminders` to show reminder from a specific category - Hadith, Duas, Quranic Duas or from all the categories

<table>
	<thead>
		<tr>
			<th>Options</th>
			<th>Description</th>
			<th>Default</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><code>updateInterval</code></td>
			<td>How often a new reminder gets displayed. Value is in SECONDS.</td>
			<td><code>300</code></td>
		</tr>
		<tr>
			<td><code>fadeSpeed</code></td>
			<td>How fast (in SECONDS) to fade out and back in when changing reminders.</td>
			<td><code>4</code></td>
		</tr>
		<tr>
			<td><code>category</code></td>
			<td>What category to pick from. The <code>random</code> setting will pick a random reminder out of all the available categories. Or you can set it to a specific category: <code>Hadiths</code>, <code>Duas</code> or <code>QuranicDuas</code>.</td>
			<td><code>random</code></td>
		</tr>
        <tr>
			<td><code>remoteFile</code></td>
			<td>File name of your own Islamic Reminders</td>
			<td><code>IslamicReminders.json</code></td>
		</tr>
		
	</tbody>
</table>

### Example Configuration
Simplest Configuration:
````javascript
modules: [
    ... // Other modules
    
    {
        module: "mmIslamicReminders",
        position: "lower_third",
    },

    ... // Other modules
]
````

Granular configuration:
````javascript
modules: [
    ... // Other modules
    
    {
        module: "mmIslamicReminders",
        position: "lower_third",
        config: {
                updateInterval: 300,
                fadeSpeed: 4,
                category: "Duas",
                remoteFile: "IslamicReminders.json",
        }
    },
    
    ... // Other modules
]
````

## Updating Reminders
It is recommended to update your own collection of aHadith/Duas in the Islamicreminders.json file rather than in the mmIslamicReminders.js. The format of the json file is as described below

````json
{
    "Hadiths": [
        "The Prophet (saws) ordered the people to pay Zakat-ul-Fitr before going to the Eid prayer. ~ Sahih Bukhari",
        "The Prophet (saws) said: \"Do Wudu properly.\" ~ Sunan An-Nasai"
    ],
    "Duas": [
        "سُبْحَانَ اللهِ، والحَمْدُ للهِ، وَ لاَ إِلَهَ إلاَّ اللهُ واللهُ أَكْبَرُ ~ At-Tirmidhi: 3509",
        "لَا حَوْلَ وَلَا قُوَّةَ إِلَّا بِاللهِ ~ Al-Bukhari 1:152"
    ],
    "QuranicDuas": [
        "رَبَّنَا تَقَبَّلْ مِنَّا إِنَّكَ أَنْتَ السَّمِيعُ العَلِيمُ ~ Surah Al-Baqarah - 2:127",
        "رَبَّنَا أَفْرِغْ عَلَيْنَا صَبْراً وَثَبِّتْ أَقْدَامَنَا وَانصُرْنَا عَلَى القَوْمِ الكَافِرِينَ ~ Surah Al-Baqarah - 2:250"
    ]
}
```` 
### Note: 
The json file is very sensitive and care should be taken for the following points:

1. Do not end the last item in the array or the last array with a comma `,`
2. Make sure each item is enclosed in double quotes `"` ex. `"لَا حَوْلَ وَلَا قُوَّةَ إِلَّا بِاللهِ ~ Al-Bukhari 1:152"`
3. Make sure to escape double quotes with in an item, instead `"` use `\"` ex. `"The Prophet (saws) said: \"Do Wudu properly.\" ~ Sunan An-Nasai"`



## TODO
1. Specify font through CSS.